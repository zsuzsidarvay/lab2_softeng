USE master;
GO

IF EXISTS(select * from sys.databases where name='Vendeglok')
	DROP DATABASE Vendeglok

SET DATEFORMAT YMD

CREATE DATABASE Vendeglok;
GO

USE Vendeglok;
GO

DROP TABLE IF EXISTS FoglalasTulaj
DROP TABLE IF EXISTS Vendegek
DROP TABLE IF EXISTS Vendeglo
DROP TABLE IF EXISTS Felhasznalok
DROP TABLE IF EXISTS Foglalas

CREATE TABLE Vendeglo(
	VendegloNev VARCHAR(100),
	Varos VARCHAR(100),
	Utca VARCHAR(100),
	Hazszam INT,
	Telefonszam VARCHAR(100),
	Nyitas VARCHAR(100),
	Zaras VARCHAR(100),
	Kep VARCHAR(100),

	CONSTRAINT PK_Vendeglok PRIMARY KEY(VendegloNev)
);


insert into Vendeglo (VendegloNev, Varos, Utca, Hazszam, Telefonszam, Nyitas, Zaras)values ('Bulgakov', 'Barcelona','Valala', 163,'08036','10:05', '15:30'),
                          ('Chios','Kolozsvar','Central', 15,'0753059361','12:00', '18:00'),
                          ('Cafe','Nairobi','Hulala',3,'589234','5:00', '22:00')


CREATE TABLE Vendegek(
	VendegNev VARCHAR(100),
	Email VARCHAR(100),
	Telefonszam VARCHAR(100),

	CONSTRAINT PK_Felhasznalok PRIMARY KEY(VendegNev)
);



insert into Vendegek( VendegNev, Email, Telefonszam) values ('Zsuzsi', 'zsuzsi@gmail.com','0758774903'),
                          ('Pista','oista@yahoo.com','333333'),
                          ('Marci','marcika@gmail.com', '11122244')


CREATE TABLE Felhasznalok (
	FelhID INT IDENTITY,
	Felhasznalonev VARCHAR(100),
	Jelszo VARCHAR(100),
	FelhCsop VARCHAR(100),
	CONSTRAINT FK_FK PRIMARY KEY(FelhID),
	CONSTRAINT Felhasznalonev_unique UNIQUE(Felhasznalonev)
);

CREATE TABLE Foglalas(
	FoglalasID INT IDENTITY,
	VendegloNev VARCHAR(100),
	FelhasznaloNev VARCHAR(100),
	FelhasznaloLogged VARCHAR(100),

	CONSTRAINT PK_Foglalas PRIMARY KEY(FoglalasID),
	CONSTRAINT FK_Foglalas_Vendeglo FOREIGN KEY(VendegloNev) REFERENCES Vendeglo(VendegloNev),
	CONSTRAINT FK_Foglalas_Felhasznalo FOREIGN KEY(FelhasznaloNev) REFERENCES Vendegek(VendegNev),
	CONSTRAINT FK_Foglalas_FelhID FOREIGN KEY(FelhasznaloLogged) REFERENCES Felhasznalok(Felhasznalonev)
);

SET IDENTITY_INSERT Foglalas ON

insert into Foglalas(FoglalasID,VendegloNev, FelhasznaloNev)  values (100,'Chios','Zsuzsi'),
                          (101,'Chios', 'Pista'),
						  (102, 'Bulgakov', 'Zsuzsi')

SET IDENTITY_INSERT Foglalas OFF




