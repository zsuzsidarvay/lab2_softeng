// vendeg middleware
module.exports = (req, res, next) => {
  const fcsop = req.session.felhCsop;
  if (fcsop === 'adminisztrator' || fcsop === 'vendeg')   {
    console.log('vendeg vagy adminisztrator');
    next();
  } else {
    res.status(401).render('error', { message: 'Unauthorized access', session: req.session });
  }
};
