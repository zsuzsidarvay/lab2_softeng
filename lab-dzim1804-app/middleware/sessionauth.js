// session auth middleware
module.exports = (req, res, next) => {
  if (req.session) {
    if (req.session.user) {
      console.log('session auth middleware');
      console.log(req.session.user);
      next();
    } else {
      console.log('session auth middleware not logged in');
      next();
    }
  } else {
    console.log('session auth middleware: nincs session');
    next();
  }
};
