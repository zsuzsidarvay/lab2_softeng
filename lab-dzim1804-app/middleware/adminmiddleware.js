// adminisztrator middleware
module.exports = (req, res, next) => {
  const fcsop = req.session.felhCsop;
  if (fcsop === 'adminisztrator')  {
    console.log('adminisztrator');
    next();
  } else {
    res.status(401).render('error', { message: 'Unauthorized access', session: req.session });
  }
};
