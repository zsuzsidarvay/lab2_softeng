// Hasznalat: A vendeglo.sql (mssql) allomanyt kell futtatni

const express = require('express'),
  path = require('path'),
  ehandlebars = require('express-handlebars'),
  session = require('express-session'),
  errorMiddleware = require('./middleware/error'),
  sessionAuthMiddleware = require('./middleware/sessionauth'),
  vendegMiddleware = require('./middleware/vendegmiddleware'),
  adminMiddleware = require('./middleware/adminmiddleware'),
  vendeglokRoutes = require('./routes/vendeglok'),
  foglalasRoutes = require('./routes/foglalas'),
  uploadRoutes = require('./routes/upload'),
  bevezetRoutes = require('./routes/bevezet'),
  vendeglolistaRoutes = require('./routes/vendegloklista'),
  submitRoutes = require('./routes/submit_form'),
  loginRoutes = require('./routes/login'),
  logoutRoutes = require('./routes/logout'),
  regisztralRoutes = require('./routes/regisztracio'),
  submitRegisztralRoutes = require('./routes/submit_regisztral'),
  torolRoutes = require('./routes/torol'),
  vendegloReszletekRoutes = require('./routes/vendegloreszletek');


const app = express();

// a static mappából adjuk a HTML állományokat
app.use(express.static(path.join(__dirname, 'static')));

// beállítjuk a handlebars-t, mint sablonmotor
app.set('view engine', 'hbs');
app.set('views', path.join(__dirname, 'views'));
app.engine('hbs', ehandlebars({
  extname: 'hbs',
  defaultView: 'main',
  layoutsDir: path.join(__dirname, 'views/layouts'),
  partialsDir: path.join(__dirname, 'views/partials'),
}));

// standard kérésfeldolgozással kapjuk a body tartalmát
app.use(express.urlencoded({ extended: true }));

app.use(session({
  secret: '142e6ecf42884f03',
  resave: false,
  saveUninitialized: true,
}));

app.use(sessionAuthMiddleware);

app.use('/submit_reszletek', vendegloReszletekRoutes);
app.use('/torol', torolRoutes);
app.use('/regisztracio', regisztralRoutes);
app.use('/submit_regisztral', submitRegisztralRoutes);
app.use('/login', loginRoutes);
app.use('/logout', logoutRoutes);
app.use('/', vendeglolistaRoutes);
app.use('/kepfeltoltes', vendeglokRoutes);

app.use(vendegMiddleware);

app.use('/foglalas', foglalasRoutes);
app.use('/upload', uploadRoutes);

app.use(adminMiddleware);

app.use('/bevezet', bevezetRoutes);
app.use('/submit_form', submitRoutes);

app.use(errorMiddleware);

app.listen(8000, () => {
  console.log('Server listening...');
});
