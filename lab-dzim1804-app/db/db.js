// Adatbázis műveleteket végző modul

const sql = require('mssql');

// Létrehozunk egy connection poolt
const connectionString = 'mssql://sa:D0cker2*2*@v-db/Vendeglok';


const pool = new sql.ConnectionPool(connectionString);
pool.connect((err) => {
  if (err) {
    console.error(`Connection error: ${err.message}`);
    process.exit(1);
  }
  console.log('Connected to Vendeglok');
  const query = 'SELECT * FROM Vendeglo';

  pool.request().query(query, (err2) => {
    if (err2) {
      console.error(`Error: ${err2.message}`);
      process.exit(1);
    } else {
      console.log('Lekerdezes rendben.');
    }
  });
});


exports.insertInn = (req, callback) => {
  const query = `INSERT INTO Vendeglo (VendegloNev, Varos, Utca, Hazszam, Telefonszam, Nyitas, Zaras) VALUES (
    '${req[0]}', '${req[1]}', '${req[2]}', '${req[3]}', '${req[4]}', '${req[5]}', '${req[6]}')`;
  pool.request().query(query, callback);
};


exports.findAllInn = (callback) => {
  const query = 'SELECT * FROM Vendeglo';
  pool.request().query(query, (err, data) => callback(err, 'recordset' in data ? data.recordset : []));
};

exports.findOneInn = (vendeglonev, callback) => {
  const query = `SELECT * FROM Vendeglo WHERE VendegloNev = '${vendeglonev}'`;
  pool.request().query(query, (err, data) => callback(err, 'recordset' in data ? data.recordset[0] : []));
};

exports.findKep = (vendeglonev, callback) => {
  const query = `SELECT Kep FROM Vendeglo WHERE VendegloNev = '${vendeglonev}'`;
  pool.request().query(query, (err, data) => callback(err, 'recordset' in data ? data.recordset : []));
};


// service metódus - törli az összes sort
exports.deleteAllInn = (callback) => {
  const query = 'DELETE FROM Vendeglo';
  pool.request().query(query, callback);
};

exports.findVendeglo = (callback) => {
  const query = 'SELECT * FROM Vendeglo WHERE VendegloNev = ?';
  pool.request().query(query, (err, data) => callback(err, 'recordset' in data ? data.recordset : []));
};

exports.insertFoglalas = (request, callback) => {
  const query = `INSERT INTO Foglalas(VendegloNev, FelhasznaloNev, FelhasznaloLogged) VALUES 
  ('${request[0]}', '${request[1]}' , '${request[2]}')`;
  pool.request().query(query, callback);
};
//

exports.updateKep = (request, callback) => {
  const query = `UPDATE Vendeglo 
  SET Kep = '${request[0]}'  
  WHERE VendegloNev = '${request[1]}'`;
  pool.request().query(query, callback);
};

exports.findVendeg = (callback) => {
  const query = 'SELECT * FROM Vendegek';
  pool.request().query(query, (err, data) => callback(err, 'recordset' in data ? data.recordset : []));
};

exports.findEgyFelhasznalo = (felhasznalonev, callback) => {
  const query = `SELECT * FROM Felhasznalok WHERE Felhasznalonev = '${felhasznalonev}'`;
  pool.request().query(query, (err, data) => callback(err, 'recordset' in data ? data.recordset[0] : []));
};

exports.insertFelhasznalo = (req, callback) => {
  const query = `INSERT INTO Felhasznalok (Felhasznalonev, Jelszo, FelhCsop) VALUES (
    '${req[0]}', '${req[1]}', '${req[2]}')`;
  pool.request().query(query, callback);
};


exports.findFoglalas = (foglalas, callback) => {
  const query = `SELECT * FROM Foglalas WHERE VendegloNev = '${foglalas[0]}' 
  AND FelhasznaloLogged = '${foglalas[1]}'`;
  pool.request().query(query, (err, data) => callback(err, 'recordset' in data ? data.recordset : []));
};

exports.deleteFoglalas = (foglalas, callback) => {
  const query = `DELETE FROM Foglalas WHERE FoglalasID = '${foglalas}' `;
  pool.request().query(query, callback);
};
