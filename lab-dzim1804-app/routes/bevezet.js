// Moduláris express router létrehozása

const express = require('express');

const router = express.Router();

router.get(['/', '/bevezet'], (req, res) => {
  res.render('bevezet', { session: req.session });
});

module.exports = router;
