
const express = require('express');

const router = express.Router();

router.get(['/', '/regisztral'], (req, res) => {
  res.render('regisztracio', { session: req.session });
});


module.exports = router;
