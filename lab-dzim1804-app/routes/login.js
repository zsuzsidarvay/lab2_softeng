
const express = require('express'),
  crypto = require('crypto'),
  db = require('../db/db');

const router = express.Router();

router.use(express.urlencoded({ extended: true }));

router.get(['/', '/login'], (req, res) => {
  res.render('login', { session: req.session });
});

// belépés
router.post(['/', '/login'], (req, res) => {
  const user = String(req.body.user);
  const jelszo = String(req.body.password);
  const help = [user, jelszo];
  const hashSize = 32,
    saltSize = 16,
    hashAlgorithm = 'sha512',
    iterations = 1000;
  let expectedHash = '';
  let salt = Buffer.alloc(saltSize);
  let hashWithSalt = '';

  console.log(help);

  db.findEgyFelhasznalo(user, (err, userFromDb) => {
    if (err) {
      res.status(500).render('error', { message: `Wrong username: ${err.message}` });
    } else {
      console.log(userFromDb);
      hashWithSalt = userFromDb.Jelszo;
      expectedHash = hashWithSalt.substring(0, hashSize * 2);
      salt = Buffer.from(hashWithSalt.substring(hashSize * 2), 'hex');
      console.log(salt);


      crypto.pbkdf2(jelszo, salt, iterations, hashSize, hashAlgorithm, (cryptErr, hash) => {
        if (cryptErr) {
          res.status(500).render('error', { message: `Hashing unsuccessful: ${cryptErr.message}`, session: req.session });
        } else {
          console.log(expectedHash);
          console.log(hash);
          if (expectedHash === hash.toString('hex')) {
            req.session.user = user;
            req.session.felhCsop = userFromDb.FelhCsop;

            console.log(req.session);
            res.send('Login successful');
          } else {
            res.status(401).send('Wrong credentials');
          }
        }
      });
    }
  });
});

// kilépés
router.post('/logout', (req, res) => {
  req.session.destroy((err) => {
    if (err) {
      res.status(500).send(`Session reset error: ${err.message}`);
    } else {
      res.send('Logout successful');
    }
  });
});


module.exports = router;
