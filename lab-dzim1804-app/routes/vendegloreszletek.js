// Moduláris express router létrehozása

const express = require('express'),
  eformidable = require('express-formidable'),
  db = require('../db/db');

const router = express.Router();

// formidable-lel dolgozzuk fel a kéréseket
router.use(eformidable());

// formfeldolgozás
router.post(['/', '/submit_reszletek'], (request, response) => {
  const vendeglonev = request.fields.name;
  console.log(vendeglonev);
  db.findOneInn(vendeglonev, (err, inn) => {
    if (err) {
      response.status(500).render('error', { message: `Selection unsuccessful: ${err.message}`, session: request.session });
    } else {
      console.log(inn);
      let respBody = `Város: ${inn.Varos}\n`;
      respBody += `Utca: ${inn.Utca}\n`;
      respBody += `Házszám: ${inn.Hazszam}\n`;
      respBody += `Telefonszám: ${inn.Telefonszam}`;
      console.log(respBody);

      response.set('Content-Type', 'text/plain;charset=utf-8');
      response.end(respBody);
    }
  });
});

module.exports = router;
