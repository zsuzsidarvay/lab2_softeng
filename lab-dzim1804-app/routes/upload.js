// Moduláris express router létrehozása

const express = require('express'),
  eformidable = require('express-formidable'),
  path = require('path'),
  fs = require('fs'),
  db = require('../db/db');

const router = express.Router();

const uploadDir = path.join(__dirname, '../static/uploadDir');

if (!fs.existsSync(uploadDir)) {
  fs.mkdirSync(uploadDir);
}

router.use(eformidable({ uploadDir }));

router.post(['/', '/upload'], (request, response) => {
  const fileHandler = request.files.file;
  const uploadName = path.parse(fileHandler.path).base;
  const a = request.query.id;
  const help = [uploadName, a];
  console.log(help);


  db.updateKep(help, (err) => {
    if (err) {
      response.status(500).render('error', { message: `Insertion unsuccessful: ${err.message}`, session: request.session });
    } else {
      response.redirect('/');
    }
  });
});

module.exports = router;
