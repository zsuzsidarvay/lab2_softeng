// Moduláris express router létrehozása

const express = require('express'),
  db = require('../db/db');

const router = express.Router();


router.get(['/'], (req, res) => {
  const a = req.query.id;
  // const user = req.session.user;
  const foglal = [a, req.session.user];
  console.log(a);
  console.log(foglal);
  db.findOneInn(a, (err, inn) => {
    if (err) {
      res.status(500).render('error', { message: `Selection unsuccessful: ${err.message}`, session: req.session });
    } else {
      console.log(inn);
      db.findFoglalas(foglal, (err2, foglalas) => {
        if (err2) {
          res.status(500).render('error', { message: `Selection unsuccessful: ${err2.message}`, session: req.session });
        } else {
          console.log(foglalas);
          res.render('kepfeltoltes', { inn, foglalas, session: req.session });
        }
      });
    }
  });
});

module.exports = router;
