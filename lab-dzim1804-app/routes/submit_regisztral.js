const express = require('express'),
  crypto = require('crypto'),
  db = require('../db/db');

const router = express.Router();

function insert(response, request, adatok) {
  db.insertFelhasznalo(adatok, (err) => {
    if (err) {
      response.status(500).render('error', { message: `Insertion unsuccessful: ${err.message}`, session: request.session });
    } else {
      response.redirect('/');
    }
  });
}

function kripto(response, request, jelszo, nev, felhcsop) {
  const hashSize = 32,
    saltSize = 16,
    hashAlgorithm = 'sha512',
    iterations = 1000;
  let hashWithSalt = '';
  // só
  crypto.randomBytes(saltSize, (err2, salt) => {
    if (err2) {
      response.status(500).render('error', { message: `Salt generation unsuccessful: ${err2.message}`, session: request.session });
    }
    crypto.pbkdf2(jelszo, salt, iterations, hashSize, hashAlgorithm, (cryptErr, hash) => {
      if (cryptErr) {
        response.status(500).render('error',  { message: `Hashing unsuccessful: ${cryptErr.message}`, session: request.session });
      } else {
        hashWithSalt = Buffer.concat([hash, salt]).toString('hex');
        console.log(hash);
        console.log(salt);
        console.log(hashWithSalt);
        const adatok = [];
        adatok[0] = nev;
        adatok[1] = hashWithSalt;
        adatok[2] = felhcsop;
        console.log(adatok);
        insert(response, request, adatok);
      }
    });
  });
}

router.post(['/', '/submit_regisztral'], (request, response) => {
  const nev = String(request.body.name);
  const felhcsop = String(request.body.felhcsop);
  const jelszo = String(request.body.password);

  db.findEgyFelhasznalo(nev, (err, userFromDb) => {
    if (err) {
      response.status(500).render('error', { message: `Wrong username: ${err.message}` });
    } else if (userFromDb)  {
      response.status(500).render('error', { message: 'Username exists' });
    } else {
      kripto(response, request, jelszo, nev, felhcsop);
    }
  });
});

module.exports = router;
