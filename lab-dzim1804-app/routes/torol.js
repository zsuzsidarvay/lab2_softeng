
const express = require('express'),
  eformidable = require('express-formidable'),
  bodyParser = require('body-parser'),
  db = require('../db/db');

const router = express.Router();

router.use(express.urlencoded({ extended: true }));

router.use(eformidable());

router.use(bodyParser.json());

// formfeldolgozás
router.post(['/', '/torol'], (request, response) => {
  const torles = request.fields.text;
  console.log(torles);
  db.deleteFoglalas(torles, (err) => {
    if (err) {
      response.status(500).render('error', { message: `Insertion unsuccessful: ${err.message}`, session: request.session });
    } else {
      response.json(torles);
    }
  });
});

module.exports = router;
