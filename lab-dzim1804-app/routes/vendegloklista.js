
const express = require('express'),
  db = require('../db/db');

const router = express.Router();

router.get(['/'], (req, res) => {
  db.findAllInn((err, inns) => {
    if (err) {
      res.status(500).render('error', { message: `Selection unsuccessful: ${err.message}`, session: req.session });
    } else {
      res.render('vendegloklista', { inns, session: req.session });
    }
  });
});


module.exports = router;
