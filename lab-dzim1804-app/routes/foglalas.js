// Moduláris express router létrehozása

const express = require('express'),
  db = require('../db/db');

const router = express.Router();

function uzenet(req, res) {
  db.findAllInn((err2, inns) => {
    if (err2) {
      res.render('foglalas', { message: 'Sikertelen foglalas. Kattints a foglalas linkre.', session: req.session });
      console.log('Hiba');
    } else {
      db.findVendeg((err3, user) => {
        if (err3) {
          console.log(err3);
          console.log(user);
          res.render('foglalas', { message: 'Sikertelen foglalas. Kattints a foglalas linkre.', session: req.session });
        } else {
          res.render('foglalas', {
            inns, user, session: req.session, message: 'Sikeres foglalas.',
          });
        }
      });
    }
  });
}


router.get(['/', '/foglalas'], (req, res) => {
  db.findAllInn((err, inns) => {
    if (err) {
      res.status(500).render('error', { message: `Selection unsuccessful: ${err.message}`, session: req.session });
      console.log('Hiba');
    } else {
      db.findVendeg((err2, user) => {
        if (err) {
          res.status(500).render('error', { message: `Selection unsuccessful: ${err2.message}`, session: req.session });
        } else {
          res.render('foglalas', {
            inns, user, message: 'Valassz a legordulo listakbol a foglalashoz.', session: req.session,
          });
        }
      });
    }
  });
});

router.post(['/', '/foglalas'], (req, res) => {
  const vendeglonev = String(req.body.vendeglo);
  const felhasznalonev = String(req.body.felh);
  const help = [vendeglonev, felhasznalonev, req.session.user];
  console.log(help);
  db.insertFoglalas(help, (err) => {
    if (err) {
      console.log(err);
      res.render('foglalas', { message: 'Sikertelen foglalas. Kattints a foglalas linkre.', session: req.session });
    } else {
      uzenet(req, res);
      // tulaj(req, res);
    }
  });
});


module.exports = router;
