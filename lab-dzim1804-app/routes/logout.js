const express = require('express');

const router = express.Router();

router.use(express.urlencoded({ extended: true }));

router.get(['/', '/logout'], (req, res) => {
  req.session.destroy((err) => {
    if (err) {
      res.status(500).send(`Session reset error: ${err.message}`);
    } else {
      res.send('Logout successful');
    }
  });
});

module.exports = router;
