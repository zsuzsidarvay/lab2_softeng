const express = require('express'),
  validator = require('validator'),
  db = require('../db/db');

const router = express.Router();

router.post(['/', '/submit_form'], (request, response) => {
  const vendeglok = [];
  const nev = String(request.body.name);
  const varos = String(request.body.city);
  const utca = String(request.body.street);
  const hazszam = request.body.number;
  const telefon = request.body.tel;
  const nyitvaFrom = String(request.body.from);
  const nyitvaUntil = String(request.body.until);
  if (!validator.isAlphanumeric(nev)) {
    response.status(400);
    response.end('Helytelen nev: csak szamok es betuk lehetnek');
    return;
  }
  if (!validator.isAlpha(varos)) {
    response.status(400);
    response.end('Helytelen varos: csak betuk lehetnek');
    return;
  }
  if (!validator.isAlpha(utca)) {
    response.status(400);
    response.end('Helytelen utca: csak betuk lehetnek');
    return;
  }
  if (!validator.isNumeric(hazszam)) {
    response.status(400);
    response.end('Helytelen hazszam: csak szamok lehetnek');
    return;
  } if (!validator.isNumeric(telefon)) {
    response.status(400);
    response.end('Helytelen telefon: csak szamok lehetnek');
    return;
  }
  vendeglok[0] = nev;
  vendeglok[1] = varos;
  vendeglok[2] = utca;
  vendeglok[3] = hazszam;
  vendeglok[4] = telefon;
  vendeglok[5] = nyitvaFrom;
  vendeglok[6] = nyitvaUntil;
  db.insertInn(vendeglok, (err) => {
    if (err) {
      response.status(500).render('error', { message: `Insertion unsuccessful: ${err.message}`, session: request.session });
    } else {
      response.redirect('index');
    }
  });
});

module.exports = router;
