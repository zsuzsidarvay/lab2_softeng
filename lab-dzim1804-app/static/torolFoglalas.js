
function submitForm(event) {
  // az esemény eredeti lekezelését (szinkron leadás) fölülírjuk
  event.preventDefault();
  // az esemény a formra van értelmezve
  const form = event.target;
  // kivesszük a form adatait
  const formData = new FormData(form);
  // aszinkron kérést küldünk
  fetch('/torol', {
    method: 'POST',
    body: formData,
  }).then((response) => response.json())
    .then((data) => {
      document.getElementById(`${data}`).style.display = 'none';
    });
}

submitForm();
